# SQLite 

Welcome to Android SQLite Example Tutorial. Android SQLite is the mostly preferred way to store data for android applications. For many  applications, SQLite is the apps backbone whether it’s used directly or  via some third-party wrapper. Below is the final app we will create  today using Android SQLite database.

> *<h2>Note</h2>*  This code is taken/coyed/implemented from [Journal Dev](https://www.journaldev.com/9438/android-sqlite-database-example-tutorial?unapproved=50315&moderation-hash=50b83193dc118c7cb74ddf0d17c553d4#opening-the-android-sqlite-database-file) website. If you look closer, you'll see their code is from 2015 which is almost 7 years old! So, I've just updated and modified some of codes just for learning purposes only. 

### Main Activity
![main](images/screenshot_1.png)
### Add Record
![main](images/screenshot_2.png)
### Show Database
![main](images/screenshot_3.png)
### Update Databae
![main](images/screenshot_4.png)

### Algorithm 

* **Class List :** 
    1. AddCountryActivity
    2. MainActivity
    3. DatabaseHelper
    4. DBManager
    5. ModifyCountryActivity

</br>  

* **MainActivity**[2] (MainActivity)
    * Layout - `activity_main`
    * Listview - `ModifyCountryActivity[5]` for show data 
    * onCreateOptionsMenu()
    ```
    getMenuInflater().inflate(R.menu.main, menu);
    ```
    * ActionBar - {main_menu} {res/menu}
        * `AddCountryActivity[1]`

</br>  

* **AddCountryActivity**[1]  (Add Data)
    * Layout -`activity_add_record`
        * Button (1)
        * EditText (2)

    * onCreate()
        ```
        DBManager(object).open()
        ```

    * onClick()
        ```
        button_id=add_record
        DBManager(object).insert(variable a, variable b)
        intent.start(MainActivity)
        ```

</br>  

* **ModifyCountryActivity**(5) (Update)
  
    * Layout - `activity_modify_record`      
      
        * TextView 2 
    * Button 1 
    * onClick() 
        ```
        DBManager(object).update(ver)
        DBManager(object).delete(ver)
        ```
    * returnHome()
    ```
        intent = MainActivity
    ```

</br>  

### Problems 

* How do this Alert/Fragment popup shows ?
    Add/remove this attribute on `AndroidMenifest.xml` from/on your activity..  

```xml
android:theme="@android:style/Theme.DeviceDefault.Light.Dialog"
```



### Usage: 
1. Getting Permission
```
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
```



## Changelogs 

  * v1.0 - Original Version
  * v1.1 - Little Fixes
      * Removed Unnecessary codes 
  * v1.2 - Modified 
     * Removed the old **activity_main** layout
     * Renamed **AddCountryActivity** to **MainActivity**
     * Renamed **activity_main** to **activity_main** layout 
     * Updated **activity_main** layout
  * v1.2.1 - Modified 
      * Added comments for understand codes 
      * Modified **main** and **add** layout a little! 

### TESTED AND OK

### VERY ADVANCED 

### Source

[Journal Dev](https://www.journaldev.com/9438/android-sqlite-database-example-tutorial?unapproved=50315&moderation-hash=50b83193dc118c7cb74ddf0d17c553d4#comment-50315)

